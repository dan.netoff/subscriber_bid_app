from django.shortcuts import render, redirect
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    DeleteView,
    View,
)
from django.contrib import messages

from .models import Request
from .forms import RequestCreateForm


class RequestCreateView(CreateView):
    model = Request
    fields = ['username', 'email', 'phone', 'comment', ]
    template_name = 'request/request_create.html'

    def post(self, request, *args, **kwargs):
        my_form = RequestCreateForm(request.POST)
        if my_form.is_valid():
            my_form.save()
            messages.success(request, f'Ваша Заявка отправлена!')
            return redirect('request-detail', my_form.instance.pk)
        return render(request, self.template_name, {'form': my_form})

    def get(self, request, *args, **kwargs):
        my_form = RequestCreateForm()
        return render(request, self.template_name, {'form': my_form})


class RequestDetailView(DetailView):
    model = Request


class RequestListView(ListView):
    model = Request
    template_name = 'request/home.html'
    context_object_name = 'requests'
    ordering = ['-date_add']


class RequestDeleteView(DeleteView):
    success_url = '/'
    model = Request
    success_message = "Заявка удалена!"

    def delete(self, request, *args, **kwargs):
        messages.warning(request, self.success_message)
        return super(RequestDeleteView, self).delete(request, *args, **kwargs)


class RequestUpdateView(UpdateView):
    model = Request
    fields = ['username', 'email', 'phone', 'comment', ]


class RequestChangeStatus(View):
    def get(self, request, *args, **kwargs):
        my_req = Request.objects.get(pk=kwargs['pk'])
        if my_req.status == 1:
            my_req.status = 2
            messages.success(request, f'Заявка Обработана')
        else:
            my_req.status = 1
            messages.warning(request, f'Статус заявки изменен!')
        my_req.save()
        return redirect('/')
