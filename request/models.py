from django.db import models
from django.urls import reverse


class Request(models.Model):
    username = models.CharField('Имя Абонента', max_length=50)
    email = models.CharField('Email', max_length=50)
    phone = models.CharField('Телефон', max_length=15)
    comment = models.TextField('Комментарий')
    date_add = models.DateTimeField('Создана', auto_now_add=True)
    status = models.PositiveIntegerField('Статус', default=1)

    def get_absolute_url(self):
        return reverse('request-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return 'Заявка ' + str(self.pk) + ', от' + str(self.username)

    class Meta:
        verbose_name = 'Заявки'
