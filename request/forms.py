from django import forms
from .models import Request


class RequestCreateForm(forms.ModelForm):
    email = forms.EmailField(label='Почтовый адрес', error_messages={'invalid': 'Введите корректный email', })
    phone = forms.RegexField(label='Контактный телефон',
                             regex=r'^\+?1?\d{9,15}$',
                             error_messages={'invalid': 'Формат +9999999999', }
                             )

    class Meta:
        model = Request
        fields = ['username', 'email', 'phone', 'comment', ]
