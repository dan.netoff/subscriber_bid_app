from django.contrib import admin
from django.urls import path, include

from .views import (
    RequestCreateView,
    RequestDetailView,
    RequestListView,
    RequestDeleteView,
    RequestUpdateView,
    RequestChangeStatus,
)

urlpatterns = [
    path('create/', RequestCreateView.as_view(), name='request-create'),
    path('id/<int:pk>', RequestDetailView.as_view(), name='request-detail'),
    path('id/<int:pk>/delete/', RequestDeleteView.as_view(), name='request-delete'),
    path('id/<int:pk>/update/', RequestUpdateView.as_view(), name='request-update'),
    path('id/<int:pk>/status/', RequestChangeStatus.as_view(), name='request-status'),
    path('', RequestListView.as_view(), name='request-list'),
]
